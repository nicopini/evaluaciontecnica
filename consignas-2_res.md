### ALGORITMOS

Escribir una función simple (no más de 100 caracteres) que retorne un boolean indicando si un string
dado es un palíndromo.

```javascript
  const palindromo = (str) => {return(str.substr(0,Math.floor(str.length/2))==str.substr(Math.ceil(str.length/2)).split("").reverse().join(""))}
  console.log(palindromo("anitalavalatina"));
  console.log(palindromo("larutanatural"));
```

### JAVASCRIPT

El motivo this.foo = undefined es que al definir la funcion anidada (function(){}), this queda fuera del scope del objeto myObject, no asi de la variable self definida en la outer function.

```javascript
  outer func: this.foo = bar
  outer func: self.foo = bar
  inner func: this.foo = undefined
  inner func: self.foo = bar
```
###### a. What is a Promise?

Una promesa es un objeto que representa un estado de completitud de una operacion asincrona. Como Javascript no soporta multithreading,
usualmente se utiliza una funcion callback que es ejecutada luego de finalizada una accion. Promise surge como una solucion al problema de anidacion
de callback "callback hell".
Una promesa puede existir en alguno de los siguientes estados:
- Pendiente: estado inicial.
- Cumplido: operacion completada (resolve)
- Rechazada: la operacion falla (reject)

###### b. Que es ECMAScript
ECMAScript es un estandar sobre el que Javascript esta basado. Tambien existen ActionScript y JScript
ECMA International es la organizacion encargada de definir estos estandares.

###### c. NaN
Significa "Not a Number"  ej: 1 % 0
La forma de validarlo es mediante la funcion estandar Number.isNaN(numero);

###### d. What will be the output when the following code is executed? Explain.
```javascript
console.log(false=='0')
console.log(false==='0')
```
 - == equal to: Al ser diferentes los tipos de los valores expresados en la comparacion, javascript intenta convertir uno de ellos al del otro valor. false == 0. true
 - === equal value and equal type: En este caso el operador compara tambien el tipo de ambos valores. Por ello el valor es false.

### Node

 - Explain what does "event-driven, non-blocking I/O" means in Javascript.

Arquitectura dirigida por eventos.
Es un patron de arquitectura de software que promueve la produccion, deteccion, consumo y reaccion a eventos.
Surge como solucion para evitar problemas de complejidad y colision de la programacion secuencial o estructurada.

Non blocking I/O o tambien llamado asynchronous I/O es una forma de procesamiento orientado a la E/S entrada/salida que permite continuar con el
procesamiento mientras ocurre un evento de transmision de informacion (AJAX) o cualquier evento bloqueante como por ejemplo la lectura o escritura de un archivo.
