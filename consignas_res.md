### CASO 1

 1. Resultado de las ultimas tres lineas.

 ```javascript
   f.bar(); --> Uncaught TypeError: f.bar is not a function at <anonymous>:1:3
   f.baz(); --> 7
   f.biz(); --> VM166:3 Uncaught ReferenceError: a is not defined
      at Foo.biz (<anonymous>:3:6)
      at <anonymous>:1:3
  ```

2. Modificá el código de f.bar() para que retorne 7

 ```javascript
  function Foo( a ) {
     this.a = a;
     // var baz = function() {
     //   return a;
     // };
     this.baz = function() {
       return this.a;
     };
     // agrego el metodo bar
     this.bar = function() {
       return this.a;
     }
   };
   var f = new Foo( 7 );
   f.bar();
```

3. Modificá el código para que f.biz() también retorne 7

 ```javascript
  function Foo( a ) {
     this.a = a;
     var baz = function() {
       return this.a;
     };
     this.baz = function() {
       return this.a;
     };
     // agrego el metodo bar
     this.bar = function() {
       return this.a;
     }
   };
   Foo.prototype.biz = function() {
       return this.a;
   };
   var f = new Foo( 7 );
   f.biz();
```

### CASO 2

Salida ordenada de la siguiente forma:
```javascript
[
  { skill: 'css', users: [ 'Bill', 'Sue', 'Sue' ], count: 2 },
  { skill: 'javascript', users: [ 'Chad', 'Bill', 'Sue' ], count: 3 },
  { skill: 'html', users: [ 'Sue' ], count: 1 }
]
```

```javascript
  var sortArrayBySkill = function( array ){
      var auxArray = [];
      for(var i = 0; i < array.length; i++){
          var exist = auxArray.findIndex(function(obj){
              return obj.skill == array[i].skill;
          });
          if(exist == -1){
              auxArray.push(
              { skill: array[i].skill,
                users: [ array[i].user ],
                count: 1
              });
          } else {
              auxArray[exist].users.push(array[i].user);
              auxArray[exist].count++;
          }
      }
      return auxArray;
  }  
  var endorsements = [
      { skill: 'css', user: 'Bill' },
      { skill: 'javascript', user: 'Chad' },
      { skill: 'javascript', user: 'Bill' },
      { skill: 'css', user: 'Sue' },
      { skill: 'javascript', user: 'Sue' },
      { skill: 'html', user: 'Sue' }
    ];

  var array = sortArrayBySkill(endorsements);
  array.forEach(function(obj){
      console.log(obj);
  });
```

### CASO 3

  - Implementacion funcionalidad de busqueda con promesas en lugar de callbacks.

```javascript
  const buscarEnFacebook = function(texto){
      return new Promise((resolve, reject) => {
          /* Hace algunas cosas y las guarda en "result" */
          var result = { error : false, data : texto };
          if(result.error){
              reject(new Error("result.error"));
          } else {
              resolve(result.data);
          }
      });
  }
  const buscarEnGithub = function(texto){
      return new Promise((resolve, reject) => {
          /* Hace algunas cosas y las guarda en "result" */
          var result = { error : false, data : texto };
          if(result.error){
              reject(new Error("result.error"));
          } else {
              resolve(result.data);
          }
      });
  }
  buscarEnFacebook("texto")
    .then(value => console.log("result.data: " + value))
    .catch(reason => console.log(reason));
  buscarEnGithub("bitcoin")
    .then(value => console.log("result.data: " + value))
    .catch(reason => console.log(reason));
```

  - API con promesas.

```javascript
  class buscador {
      constructor(text){
          this.text = text;
          this.arrayPromises = [];
      }
      facebook() {
          this.arrayPromises.push(new Promise((resolve, reject) => {
              /* Hace algunas cosas y las guarda en "result" */
              var result = { error : false, data : this.text };
              if(result.error){
                  reject("error de Facebook");
              } else {
                  resolve("data de Facebook");
              }
          }));
          return this;
      }
      github() {
          this.arrayPromises.push(new Promise((resolve, reject) => {
              /* Hace algunas cosas y las guarda en "result" */
              var result = { error : false, data : this.text };
              if(result.error){
                  reject("error de Github");
              } else {
                  resolve("data de Github");
              }
          }));
          return Promise.all(this.arrayPromises);
      }
  }

  new buscador('hola')
  .facebook()
  .github()
  .then((data) => {
      console.log(data);
  })
  .catch(error => {
      console.log(error);
  });
```

- y a la solución anterior
  - ¿Cómo podrías agregarle otra búsqueda?

```javascript
  class buscador {
      constructor(text){
          this.text = text;
          this.arrayPromises = [];
      }
      facebook() {
          this.arrayPromises.push(new Promise((resolve, reject) => {
              /* Hace algunas cosas y las guarda en "result" */
              var result = { error : false, data : this.text };
              if(result.error){
                  reject("error de Facebook");
              } else {
                  resolve("data de Facebook");
              }
          }));
          return this;
      }
      github() {
          this.arrayPromises.push(new Promise((resolve, reject) => {
              /* Hace algunas cosas y las guarda en "result" */
              var result = { error : false, data : this.text };
              if(result.error){
                  reject("error de Github");
              } else {
                  resolve("data de Github");
              }
          }));
          return this;
      }
      google(){
          this.arrayPromises.push(new Promise((resolve, reject) => {
              /* Hace algunas cosas y las guarda en "result" */
              var result = { error : false, data : this.text };
              if(result.error){
                  reject("error de Google");
              } else {
                  resolve("data de Google");
              }
          }));
          return Promise.all(this.arrayPromises);
      }
  }

  new buscador('hola')
  .facebook()
  .github()
  .google()
  .then((data) => {
      console.log(data);
  })
  .catch(error => {
      console.log(error);
  });
```